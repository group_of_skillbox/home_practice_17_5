#include <iostream>
#include <math.h>
using namespace std;

class Vector
{
public:

	Vector()
	{
		X = 0;
		Y = 0;
		Z = 0;
	}

	int GetX()
	{
		return X;
	}
	void SetX(int ValueX)
	{
		X = ValueX;
	}

	int GetY()
	{
		return Y;
	}
	void SetY(int ValueY)
	{
		Y = ValueY;
	}

	int GetZ()
	{
		return Z;
	}
	void SetZ(int ValueZ)
	{
		Z = ValueZ;
	}

	void ShowCoordinates()
	{
		cout << "X = " << X << "\nY = " << Y << "\nZ = " << Z << endl;
	}

	void ShowVectorModule()
	{
		cout << "Vector Module = " << sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2)) << "\n";
	}

private:
	
	int X;
	int Y;
	int Z;
};


int main()
{
	setlocale(LC_ALL, "ru");

	Vector Coordinate;

	Coordinate.SetX(5);
	Coordinate.SetY(6);
	Coordinate.SetZ(7);
	Coordinate.ShowCoordinates();
	Coordinate.ShowVectorModule();
}